package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/html"

	"github.com/bwmarrin/discordgo"
	logxi "github.com/mgutz/logxi/v1"
	"github.com/yhat/scrape"
	"golang.org/x/net/html/atom"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
)

var (
	hook   = flag.String("webhook", "", "the webhook id")
	token  = flag.String("token", "", "the webhook token")
	dsn    = flag.String("dsn", "", "the database connection string")
	area   = flag.String("area", "eu", "The area to use for the patch notes")
	logger = logxi.New("ffxiv")
	reLdst = regexp.MustCompile(`ldst_strftime\((\d+),`)
)

const DateFormat = "2006-01-02"

type XIVPosted struct {
	ID        uint64 `xorm:"pk autoincr"`
	Source    string
	Timestamp time.Time
}

type newsEntry struct {
	source    string
	url       string
	title     string
	teaser    string
	timestamp time.Time
	image     string
	tag       string
}

type newsList []newsEntry

func (l newsList) Len() int {
	return len(l)
}

func (l newsList) Less(i, j int) bool {
	return l[i].timestamp.Before(l[j].timestamp)
}

func (l newsList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func ldstToTime(script string) time.Time {
	matches := reLdst.FindStringSubmatch(script)
	if len(matches) != 2 {
		return time.Now()
	}
	timestamp, err := strconv.Atoi(matches[1])
	if err != nil {
		return time.Now()
	}

	return time.Unix(int64(timestamp), 0)
}

func fetchCategory(baseURL, path string, selector scrape.Matcher) []newsEntry {
	logger.Debug("Fetching news", "for", path)
	res, err := http.DefaultClient.Get(baseURL + path)
	if err != nil {
		logger.Warn("Failed to fetch news", "err", err)
		return nil
	}

	if res.StatusCode != 200 {
		logger.Warn("Request returned unexpected body!", "status", res.Status)
		return nil
	}

	node, err := html.Parse(res.Body)
	if err != nil {
		logger.Warn("Failed to parse response body", "err", err)
	}
	lis := scrape.FindAll(node, selector)

	entries := make([]newsEntry, len(lis))
	for idx, li := range lis {
		news := newsEntry{source: path}

		a, _ := scrape.Find(li, scrape.ByTag(atom.A))
		news.url = baseURL + scrape.Attr(a, "href")

		title, _ := scrape.Find(li, scrape.ByClass("news__list--title"))
		news.title = scrape.Text(title)

		if div, ok := scrape.Find(li, scrape.ByClass("news__list--banner")); ok {
			news.teaser = scrape.Text(div)
		}

		script, _ := scrape.Find(li, scrape.ByTag(atom.Script))
		news.timestamp = ldstToTime(scrape.Text(script))

		if img, ok := scrape.Find(li, scrape.ByTag(atom.Img)); ok {
			news.image = scrape.Attr(img, "src")
		}

		if tag, ok := scrape.Find(li, scrape.ByClass("tag")); ok {
			news.tag = strings.Trim(scrape.Text(tag), "[] ")
		}

		entries[idx] = news
	}

	return entries
}

func fetchNews(c chan *discordgo.WebhookParams, e *xorm.Engine, baseURL string) {
	topicSelector := scrape.ByClass("news__list--topics")
	newsSelector := scrape.ByClass("news__list")
	var news newsList
	colorMap := make(map[string]int)
	colorMap["/lodestone/topics"] = 0xCC8829
	// disable notices
	// colorMap["/lodestone/news/category/1"] = 0xCCCCCC
	colorMap["/lodestone/news/category/2"] = 0xCCAB29
	colorMap["/lodestone/news/category/3"] = 0x6B993D
	colorMap["/lodestone/news/category/4"] = 0x993D3D

	for {
		news = fetchCategory(baseURL, "/lodestone/topics", topicSelector)
		// disable notices
		// news = append(news, fetchCategory(baseURL, "/lodestone/news/category/1", newsSelector)...)
		news = append(news, fetchCategory(baseURL, "/lodestone/news/category/2", newsSelector)...)
		news = append(news, fetchCategory(baseURL, "/lodestone/news/category/3", newsSelector)...)
		news = append(news, fetchCategory(baseURL, "/lodestone/news/category/4", newsSelector)...)
		sort.Sort(news)

		for _, n := range news {
			m := &XIVPosted{
				Source:    n.source,
				Timestamp: n.timestamp,
			}
			if found, _ := e.Get(m); found {
				continue
			}
			e.Insert(&XIVPosted{
				Source:    n.source,
				Timestamp: n.timestamp,
			})

			wh := &discordgo.WebhookParams{
				Embeds: []*discordgo.MessageEmbed{
					&discordgo.MessageEmbed{
						Title:       n.title,
						URL:         n.url,
						Description: n.teaser,
						Color:       colorMap[n.source],
						Footer:      &discordgo.MessageEmbedFooter{Text: n.timestamp.Format(DateFormat)},
					},
				},
			}

			if n.image != "" {
				wh.Embeds[0].Image = &discordgo.MessageEmbedImage{URL: n.image}
			}

			if n.tag != "" {
				wh.Embeds[0].Description = strings.TrimSpace("**[" + n.tag + "]**\n" + wh.Embeds[0].Description)
			}

			c <- wh
		}

		time.Sleep(time.Hour)
	}
}

func main() {
	flag.Parse()
	if *hook == "" || *token == "" {
		fmt.Println("Please specify a webhook id and token!")
		os.Exit(1)
	}

	discord, _ := discordgo.New()
	wh, err := discord.WebhookWithToken(*hook, *token)
	if err != nil {
		fmt.Println("Failed to fetch webhook metadata!")
		fmt.Println(err)
		os.Exit(2)
	}

	engine, err := xorm.NewEngine("mysql", *dsn)
	if err != nil {
		fmt.Println("Failed to connect to database!")
		fmt.Println(err)
		os.Exit(3)
	}
	engine.SetMapper(new(core.GonicMapper))
	err = engine.Sync(new(XIVPosted))
	if err != nil {
		fmt.Println("Failed to sync schema to database!")
		fmt.Println(err)
		os.Exit(3)
	}

	logxi.Info("Booted FFXIV Patch Reader", "username", wh.Name)
	c := make(chan *discordgo.WebhookParams)

	go fetchNews(c, engine, "http://"+*area+".finalfantasyxiv.com")

	for {
		data, more := <-c
		if !more {
			break
		}

		logxi.Debug("New webhook request")
		err := discord.WebhookExecute(*hook, *token, true, data)
		if err != nil {
			buffer, _ := json.Marshal(data)
			logxi.Error("Failed to post webhook", "err", err, "json", string(buffer))
		} else {
			logxi.Info("Successfully posted new webhook")
		}
	}
}
